//alert(`Hello World`)

const fCube = 8 
console.log(`The cube of ${fCube} is ${fCube**3}`)

const address = ["83", "Washington Drive","23-D", "Illinois","Texas", "75216"]
console.log(`Jimmy lives at #${address[0]} ${address[1]}, Apartment ${address[2]}, ${address[3]}, ${address[4]} - ${address[5]} `)

const animal = {
	name: "Giraffe",
	origins: "South Africa",
	classM: "Mamals",
	height: "6 meters",
	weight: "1395 kg",
	status: "active"
}

const { name, origins, classM, height, weight, status } = animal 

console.log(`Originally ${name}'s are from ${origins}. This ${classM} are ${height} tall
and as heavy as ${weight}. And the fun part is they are ${status}.`)

const rankRace = [`1`,`2`,`3`,`4`,`5`]

 rankRace.forEach((n) => { console.log(n)});


class Dog{
	constructor(name, age, breed){
			this.name = name,
			this.age = age,
			this.breed = breed 
	}
}

const doggie = new Dog()
doggie.name = "Text"
doggie.age = 2
doggie.breed = "Aspin"

console.log(doggie)